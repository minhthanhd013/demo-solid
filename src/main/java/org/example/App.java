package org.example;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Coach theCoach = new BaseballCoach(12);
        System.out.println(theCoach.getDailyJob());
        System.out.println(theCoach.getSalary());

        Coach theCoach2= new FootballCoach(11);
        System.out.println(theCoach2.getDailyJob());
        System.out.println(theCoach2.getSalary());

        BaseballCoach t1 = new BaseballCoach(11);
        t1.setTheFortuneService(new BadFortune());
        System.out.println(t1.getTodayFortune());

        FootballCoach t2 = new FootballCoach(10);
        t2.setTheFortuneService(new GoodFortune());
        System.out.println(t2.getTodayFortune());

    }
}

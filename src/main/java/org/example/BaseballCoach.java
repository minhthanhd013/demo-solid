package org.example;

public class BaseballCoach implements Coach{
    private int workHours;
    private FortuneService theFortuneService;

    public String getTodayFortune() {
        return theFortuneService.getTodayFortune();
    }

    public void setTheFortuneService(FortuneService theFortuneService) {
        this.theFortuneService = theFortuneService;
    }

    public BaseballCoach(int workHours) {
        this.workHours = workHours;
    }

    @Override
    public String getDailyJob() {
        return "Practice batting for 30 minutes";
    }

    @Override
    public float getSalary() {
        return workHours*2;
    }
}

package org.example;

public class FootballCoach implements Coach{
    private int workHours;
    private FortuneService theFortuneService;

    public String getTodayFortune() {
        return theFortuneService.getTodayFortune();
    }

    public void setTheFortuneService(FortuneService theFortuneService) {
        this.theFortuneService = theFortuneService;
    }

    public FootballCoach(int workHours) {
        this.workHours = workHours;
    }

    @Override
    public String getDailyJob() {
        return "Run around the pitch";
    }

    @Override
    public float getSalary() {
        return workHours *  3;
    }
}

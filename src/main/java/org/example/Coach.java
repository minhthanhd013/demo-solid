package org.example;

public interface Coach{
    public String getDailyJob();
    public float getSalary();
}
